import 'dart:html';

import 'package:dart_web_chat/utils/html_widget.dart';

class ColumnLayout extends HtmlWidget {
  final HtmlWidget leftNavigationWidget;
  final HtmlWidget contentWidget;

  ColumnLayout(
      {required this.leftNavigationWidget, required this.contentWidget});

  @override
  Element build() {
    return DivElement()
      ..className = 'h-screen'
      ..children = [
        DivElement()
          ..className =
              'w-96 h-screen overscroll-y-auto border-r border-grey-300 fixed'
          ..children = [
            leftNavigationWidget.append(this),
          ],
        DivElement()
          ..className = 'ml-96'
          ..children = [
            contentWidget.append(this),
          ],
      ];
  }
}
