import 'dart:html';
import 'package:matrix/matrix.dart';
import '../dart_web_chat.dart';
import '../utils/html_widget.dart';

class Avatar extends HtmlWidget {
  final Uri? mxc;
  final String username;

  Avatar([this.mxc, this.username = 'Unknown']);

  static const String style = 'w-8 h-8 rounded-full bg-gray-200 mr-4';
  @override
  Element build() {
    final client = findParent<DartWebChat>().client;
    if (mxc != null) {
      return ImageElement(
        src: mxc.getThumbnail(client, width: 32, height: 32).toString(),
      )..className = style;
    }
    return DivElement()
      ..className = style + ' align-middle text-center'
      ..text = username.substring(0, 2);
  }
}
