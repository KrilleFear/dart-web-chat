import 'dart:html';

import '../utils/html_widget.dart';

class HashRouter extends HtmlWidget {
  final HtmlWidget Function(String) routeBuilder;
  String currentRoute;

  HashRouter({
    required this.routeBuilder,
    String? initialRoute,
  }) : currentRoute =
            initialRoute ?? window.location.hash.replaceFirst('#', '') {
    window.onHashChange.listen(_onHashChangeListener);
  }

  void _onHashChangeListener(_) {
    final route = window.location.hash.replaceFirst('#', '');
    if (route != currentRoute) push(route);
  }

  void push(String route) => setState(() {
        currentRoute = route;
      });

  @override
  Element build() {
    window.location.hash = currentRoute;
    return routeBuilder(currentRoute).append(this);
  }
}
