import 'dart:html';

import 'package:matrix/matrix.dart';

import '../utils/html_widget.dart';
import 'hash_router.dart';
import 'avatar.dart';

class RoomListItem extends HtmlWidget {
  final Room room;

  RoomListItem({required this.room});

  void goToRoom(MouseEvent event) {
    findParent<HashRouter>().push('/rooms/${room.id}');
  }

  @override
  Element build() {
    final text = room.notificationCount == 0
        ? room.displayname
        : '(${room.notificationCount}) ${room.displayname}';
    return LIElement()
      ..className =
          'flex my-2 rounded border border-transparent h-12 p-2 bg-gray-100 hover:bg-gray-300 cursor-pointer'
      ..onClick.listen(goToRoom)
      ..children = [
        Avatar(room.avatar, room.displayname).append(this)
          ..className = 'flex-initial',
        ParagraphElement()
          ..className = 'flex-initial align-middle'
          ..text = text,
      ];
  }
}
