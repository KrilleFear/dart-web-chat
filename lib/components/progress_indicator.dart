import 'dart:html';

import 'package:dart_web_chat/utils/html_widget.dart';

class LoadingIndicator extends HtmlWidget {
  final String color;

  LoadingIndicator({this.color = 'bg-white'});
  @override
  Element build() {
    return DivElement()
      ..className = 'animate-spin justify-self-center h-5 w-5 mr-3 $color';
  }
}
