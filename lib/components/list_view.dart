import 'dart:async';
import 'dart:html';
import 'dart:math';

import 'package:dart_web_chat/utils/html_widget.dart';

enum Axis { horizontal, vertical }

extension on Axis {
  String get overflowClassName {
    switch (this) {
      case Axis.horizontal:
        return 'overflow-x-scroll';
      case Axis.vertical:
        return 'overflow-y-scroll';
    }
  }
}

class ListView extends HtmlWidget {
  final bool reverse;
  final Axis? axis;
  final int itemCount;
  final int chunkSize;
  final String? style;
  final Element Function(int index, HtmlWidget parent) builder;
  final Element Function(HtmlWidget parent)? headerBuilder;
  final Element Function(HtmlWidget parent)? footerBuilder;

  late final _ListViewContent _listViewContent;

  DivElement? _scrollView;

  StreamSubscription? _onScrollSub;

  ListView({
    required this.itemCount,
    required this.builder,
    this.reverse = false,
    this.axis = Axis.vertical,
    this.headerBuilder,
    this.footerBuilder,
    this.style,
    this.chunkSize = 100,
  }) {
    _listViewContent = _ListViewContent(
      itemCount: min(chunkSize, itemCount),
      builder: builder,
      headerBuilder: headerBuilder,
      footerBuilder: footerBuilder,
      reverse: reverse,
    );
  }

  void update([int? newItemCount]) {
    if (newItemCount != null) {
      _listViewContent.itemCount = newItemCount;
    }
    _listViewContent.setState(() => null);
  }

  void _onScroll(Event event) {
    if (axis == null) {}
    final extend = (axis == null
            ? window.scrollY / window.innerHeight!
            : _scrollView!.scrollTop / _scrollView!.scrollHeight) >=
        0.8;
    if (itemCount > _listViewContent.itemCount && extend) {
      update(min(_listViewContent.itemCount + chunkSize, itemCount));
    }
  }

  @override
  Element build() {
    _scrollView ??= DivElement()
      ..className =
          'w-full h-full ${axis != null ? axis?.overflowClassName : ''}' +
              (style != null ? ' $style' : '')
      ..children = [_listViewContent.append(this)];
    _onScrollSub ??= _scrollView!.onScroll.listen(_onScroll);
    return _scrollView!;
  }
}

class _ListViewContent extends HtmlWidget {
  final bool reverse;
  int itemCount;
  final Element Function(int index, HtmlWidget parent) builder;
  final Element Function(HtmlWidget parent)? headerBuilder;
  final Element Function(HtmlWidget parent)? footerBuilder;

  _ListViewContent({
    required this.itemCount,
    required this.builder,
    this.reverse = false,
    this.headerBuilder,
    this.footerBuilder,
  });

  @override
  Element build() {
    final headerBuilder = this.headerBuilder;
    final footerBuilder = this.footerBuilder;
    return UListElement()
      ..children = [
        if (headerBuilder != null) headerBuilder(this),
        if (reverse) ...{
          for (var i = itemCount - 1; i >= 0; i--) builder(i, this),
        } else ...{
          for (var i = 0; i < itemCount; i++) builder(i, this),
        },
        if (footerBuilder != null) footerBuilder(this),
      ];
  }
}
