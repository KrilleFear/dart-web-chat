import 'dart:html';

import 'package:matrix/matrix.dart';
import 'components/progress_indicator.dart';
import 'config/app_routes.dart';
import 'utils/html_widget.dart';
import 'components/hash_router.dart';

class DartWebChat extends HtmlWidget {
  final Client client;
  bool initialized = false;

  DartWebChat()
      : client = Client(
          'DartWebChat',
          databaseBuilder: (Client client) async {
            final db = FamedlySdkHiveDatabase(client.clientName);
            await db.open();
            return db;
          },
        ) {
    client.init().then((_) => setState(() => initialized = true));
  }

  @override
  Element build() {
    if (!initialized) {
      return DivElement()
        ..className = 'w-screen h-screen'
        ..children = [LoadingIndicator(color: 'bg-purple-400').append(this)];
    }
    return HashRouter(
      initialRoute: '/',
      routeBuilder: AppRoutes(client).routeBuilder,
    ).append(this);
  }
}
