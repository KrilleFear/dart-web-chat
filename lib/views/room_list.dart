import 'dart:async';
import 'dart:html';

import 'package:dart_web_chat/components/list_view.dart';
import 'package:dart_web_chat/views/room_list_view.dart';
import 'package:matrix/matrix.dart';

import '../dart_web_chat.dart';
import '../utils/html_widget.dart';
import '../components/hash_router.dart';

class RoomListView extends HtmlWidget {
  bool logoutLoading = false;
  void logout(MouseEvent event) async {
    setState(() => logoutLoading = true);
    try {
      await findParent<DartWebChat>().client.logout();
      findParent<HashRouter>().push('/');
    } catch (e) {
      window.alert(e.toString());
    } finally {
      setState(() => logoutLoading = false);
    }
  }

  StreamSubscription? _onUpdateSub;

  ListView? list;

  void _onUpdate(SyncUpdate syncUpdate) {
    if (!mounted) {
      _onUpdateSub?.cancel();
      return;
    }
    list?.update(findParent<DartWebChat>().client.rooms.length);
  }

  @override
  Element build() {
    final client = findParent<DartWebChat>().client;
    _onUpdateSub ??= client.onSync.stream.listen(_onUpdate);
    return buildRoomListView(this);
  }
}
