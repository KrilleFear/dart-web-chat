import 'dart:html';

import '../utils/html_widget.dart';
import '../components/hash_router.dart';

class ViewEins extends HtmlWidget {
  void toViewZwei(_) {
    findParent<HashRouter>().push('/');
  }

  @override
  Element build() {
    return DivElement()
      ..className = 'container'
      ..children = [
        BRElement(),
        ParagraphElement()..innerText = 'Page not found'
      ];
  }
}
