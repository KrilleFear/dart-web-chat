import 'dart:html';

import 'package:dart_web_chat/components/list_view.dart';
import 'package:dart_web_chat/config/themes.dart';
import 'package:dart_web_chat/views/room_view.dart';
import 'package:matrix/matrix.dart';

import '../dart_web_chat.dart';
import '../utils/html_widget.dart';

class RoomView extends HtmlWidget {
  final String roomId;
  Timeline? timeline;

  final InputElement inputTextField = InputElement()
    ..className = Themes.textFieldStyle + ' flex-auto mr-2'
    ..autocomplete = 'off';

  RoomView({required this.roomId});

  Room get room => findParent<DartWebChat>().client.getRoomById(roomId)!;
  void goBackAction(MouseEvent event) {
    window.history.back();
  }

  void sendMessage(_) {
    final message = inputTextField.value?.trim();
    if (message?.isEmpty ?? true) return;
    room.sendTextEvent(inputTextField.value);
    inputTextField.value = '';
  }

  void _onUpdate() {
    if (!mounted) return;
    if (room.notificationCount != null &&
        room.notificationCount > 0 &&
        timeline != null &&
        timeline!.events.isNotEmpty) {
      room.setReadMarker(
        timeline!.events.first.eventId,
        readReceiptLocationEventId: timeline!.events.first.eventId,
      );
    }
    list?.update(timeline!.events.length);
  }

  ListView? list;

  @override
  Element build() {
    final room = findParent<DartWebChat>().client.getRoomById(roomId)!;
    if (timeline == null) {
      room
          .getTimeline(onUpdate: _onUpdate)
          .then((timeline) => setState(() => this.timeline = timeline));
    }
    return buildRoomView(this);
  }
}
