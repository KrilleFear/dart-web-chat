import 'dart:html';

import 'package:dart_web_chat/components/list_view.dart';
import 'package:dart_web_chat/components/progress_indicator.dart';
import 'package:dart_web_chat/components/room_list_item.dart';
import 'package:dart_web_chat/config/themes.dart';
import 'package:dart_web_chat/utils/html_widget.dart';
import 'package:dart_web_chat/views/room_list.dart';

import '../dart_web_chat.dart';

Element buildRoomListView(RoomListView view) {
  final client = view.findParent<DartWebChat>().client;
  view.list ??= ListView(
    headerBuilder: (_) =>
        HeadingElement.h1()..text = 'Logged in as ${client.userID}',
    itemCount: client.rooms.length,
    builder: (int i, HtmlWidget parent) =>
        RoomListItem(room: client.rooms[i]).append(parent),
    style: 'p-2 overflow-x-hidden',
    footerBuilder: (HtmlWidget parent) => ButtonElement()
      ..disabled = view.logoutLoading
      ..className = Themes.buttonStyle + ' w-full'
      ..children = [
        if (view.logoutLoading) LoadingIndicator().append(parent),
        if (!view.logoutLoading) ParagraphElement()..text = 'Logout',
      ]
      ..onClick.listen(view.logout),
  );
  return view.list!.append(view);
}
