import 'dart:html';

import 'package:dart_web_chat/components/list_view.dart';
import 'package:dart_web_chat/config/themes.dart';
import 'package:dart_web_chat/utils/html_widget.dart';
import 'package:dart_web_chat/views/room.dart';

Element buildRoomView(RoomView view) {
  if (view.timeline != null) {
    view.list ??= ListView(
      axis: null,
      itemCount: view.timeline!.events.length,
      builder: (int i, HtmlWidget parent) {
        final event = view.timeline!.events[i];
        return ParagraphElement()
          ..className =
              '${event.status != 2 ? 'opacity-50' : event.status == -1 ? 'text-red-700' : ''}'
          ..text =
              '${event.sender.calcDisplayname()} [${event.originServerTs.toIso8601String()}]: ${event.body}';
      },
      reverse: true,
      style: 'flex-none mt-12 mb-12',
    );
  }
  return DivElement()
    ..className = 'container p-2 h-screen'
    ..children = [
      DivElement()
        ..className = 'flex fixed'
        ..children = [
          ButtonElement()
            ..text = 'Back'
            ..className = Themes.buttonStyle + ' flex-initial mr-2'
            ..onClick.listen(view.goBackAction),
          ParagraphElement()
            ..className = 'flex-initial align-center h-full'
            ..text = view.room.displayname,
        ],
      HRElement(),
      if (view.timeline != null) view.list!.append(view),
      HRElement(),
      FormElement()
        ..className = 'flex fixed bottom-0 right-0'
        ..setAttribute('onsubmit', 'return false')
        ..onSubmit.listen(view.sendMessage)
        ..children = [
          view.inputTextField,
          ButtonElement()
            ..className = Themes.buttonStyle + ' flex-auto'
            ..text = 'Send',
        ],
    ];
}
