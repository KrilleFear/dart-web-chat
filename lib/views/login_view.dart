import 'dart:html';

import 'package:dart_web_chat/components/progress_indicator.dart';
import 'package:dart_web_chat/config/themes.dart';
import 'package:dart_web_chat/views/login.dart';

Element buildLoginView(LoginView view) {
  return DivElement()
    ..className = 'container mx-auto max-w-screen-sm px-4 py-4'
    ..children = [
      HeadingElement.h1()
        ..className = 'my-2 text-2xl font-bold'
        ..text = 'Dart Web Chat :-)',
      FormElement()
        ..onSubmit.listen(view.loginAction)
        ..children = [
          view.homeserverTextField
            ..type = 'url'
            ..className = Themes.textFieldStyle
            ..placeholder = 'https://homeserver'
            ..readOnly = view.loading,
          view.usernameTextField
            ..type = 'text'
            ..autocomplete = 'off'
            ..className = Themes.textFieldStyle
            ..placeholder = 'Username'
            ..readOnly = view.loading,
          view.passwordTextField
            ..type = 'password'
            ..autocomplete = 'off'
            ..className = Themes.textFieldStyle
            ..placeholder = 'Password'
            ..readOnly = view.loading,
          if (view.error != null)
            ParagraphElement()
              ..className = 'text-red-600 my-2'
              ..text = view.error,
          ButtonElement()
            ..disabled = view.loading
            ..className = Themes.buttonStyle + ' w-full'
            ..children = [
              if (view.loading) LoadingIndicator().append(view),
              if (!view.loading) ParagraphElement()..text = 'Login',
            ],
        ],
    ];
}
