import 'dart:html';

import 'package:dart_web_chat/views/login_view.dart';
import 'package:matrix/matrix.dart';

import '../dart_web_chat.dart';
import '../utils/html_widget.dart';
import '../components/hash_router.dart';

class LoginView extends HtmlWidget {
  String? textFieldValue;
  bool loading = false;
  String? error;

  final InputElement homeserverTextField = InputElement();

  final InputElement usernameTextField = InputElement();

  final InputElement passwordTextField = InputElement();

  void loginAction(_) async {
    setState(() {
      loading = true;
      error = null;
    });
    try {
      await findParent<DartWebChat>()
          .client
          .checkHomeserver(homeserverTextField.value);
      await findParent<DartWebChat>().client.login(
            identifier:
                AuthenticationUserIdentifier(user: usernameTextField.value),
            password: passwordTextField.value,
          );
      setState(() {
        loading = false;
      });
      findParent<HashRouter>().push('/');
    } catch (e) {
      setState(() {
        loading = false;
        error = e.toString();
      });
    }
  }

  @override
  Element build() => buildLoginView(this);
}
