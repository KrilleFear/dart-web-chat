import 'dart:html';

abstract class HtmlWidget {
  late final HtmlWidget? parent;

  Element build();

  Element append(HtmlWidget parent) {
    this.parent = parent;
    return wrapWithElement();
  }

  T findParent<T>() {
    final parentPointer = parent;
    if (parentPointer == null) {
      throw Exception('Unable to find parent of type $T in widget tree');
    }
    if (parentPointer is T) {
      return parentPointer as T;
    }
    return parentPointer.findParent<T>();
  }

  void setState(void Function() fun) {
    fun();
    final widgetNode =
        document.querySelector('[$_dataWidgetTypeId="${hashCode.toString()}"]');
    if (widgetNode == null) {
      throw Exception(
          'No widget node with hashCode $hashCode found in the DOM!');
    }
    widgetNode.children = [build()];
  }

  bool get mounted =>
      document.querySelector('[$_dataWidgetTypeId="${hashCode.toString()}"]') !=
      null;

  @override
  String toString() => build().toString();
}

const String _dataWidgetTypeKey = 'data-widget-type';
const String _dataWidgetTypeId = 'data-widget-id';

extension _WrapWithElement on HtmlWidget {
  Element wrapWithElement() => SpanElement()
    ..setAttribute(_dataWidgetTypeKey, runtimeType.toString())
    ..setAttribute(_dataWidgetTypeId, hashCode.toString())
    ..children = [build()];
}

void runApp(HtmlWidget widget, {String targetId = 'app'}) {
  final appNode = document.getElementById(targetId);
  if (appNode == null) {
    throw Exception('There is no element with the ID $targetId in the DOM!');
  }
  appNode.children = [widget.wrapWithElement()];
}
