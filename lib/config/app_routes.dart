import 'package:dart_web_chat/components/column_layout.dart';
import 'package:dart_web_chat/utils/html_widget.dart';
import 'package:dart_web_chat/views/login.dart';
import 'package:dart_web_chat/views/room.dart';
import 'package:dart_web_chat/views/room_list.dart';
import 'package:dart_web_chat/views/view_eins.dart';
import 'package:matrix/matrix.dart';

class AppRoutes {
  final Client client;

  AppRoutes(this.client);

  HtmlWidget routeBuilder(String route) {
    if (!client.isLogged()) return LoginView();
    final parts = route.split('/');

    if (route == '/') {
      return ColumnLayout(
        leftNavigationWidget: RoomListView(),
        contentWidget: ViewEins(),
      );
    }
    if (parts.length == 3 && parts[1] == 'rooms') {
      return ColumnLayout(
        leftNavigationWidget: RoomListView(),
        contentWidget: RoomView(roomId: parts[2]),
      );
    }
    return ViewEins();
  }
}
