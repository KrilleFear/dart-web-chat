abstract class Themes {
  static const String buttonStyle =
      'group relative flex justify-center shadow py-2 px-4 my-2 border border-transparent text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500';
  static const String textFieldStyle =
      'shadow appearance-none border rounded w-full py-2 px-3 my-2 text-grey-darker';
}
