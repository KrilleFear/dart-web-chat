# Dart Web Chat

Simple playground for using pure Dart on the web by building a minimal Matrix client using the matrix package from pub.dev.

## How to build

### Dependencies

This project uses tailwindcss. Create the tailwind.css file with:

```
npx tailwindcss -o web/tailwind.css
```

You also need `olm.js` for the end-to-end encryption. You can import it with:

```
curl -L 'https://gitlab.com/famedly/libraries/olm/-/jobs/artifacts/master/download?job=build_js' > web/olm.zip
unzip web/olm.zip
mv javascript/* web/
rm -rf javascript
rm web/olm.zip
```

You also need to install dart:

https://dart.dev

### Run project directly

Start the project with Dart web dev. Get started with it here:

https://dart.dev/tutorials/web/get-started

And then execute: 

```
webdev serve
```

### Build web app

Transpile the dart code to minified JavaScript:

```
dart pub get
dart compile js ./web/main.dart -o ./web/main.dart.js -m
```

Then you can export the `web/` directory.
